# Docbot


DocBot is a Pneumonia Detection chatbot that was developed during the SRM Hackathon held on the 12th and 13th of October.

  - Diagnoses whether the patient has Pneumonia or not based on Image Classification of thier Chest X-Ray
  - Gives the prediction along with a Confidence score that signifies with what confidence the prediction is made by the Model
  - Demo Video Link :- https://drive.google.com/file/d/16na61XPOziYOPF8LV6IvS09Jez23HQjh/view?usp=drivesdk

### Tech

DocBot uses the following Frameworks for its functioning:

* [Android Studio](https://developer.android.com/studio/?gclid=CjwKCAiAiuTfBRAaEiwA4itUqA3cR_EnnFaAEAM9sTBv4SttXMKv8ADXascp0LtobJb4dTuTFz9iCRoC71kQAvD_BwE) - Android application creation framework
* [Keras](https://keras.io/) - Python based Neural network library
* [Flask](http://flask.pocoo.org/) - Python based Web development framework.
* [OpenCV](https://opencv.org/) - Computer Vision framework used for Image processing
* [Tensorflow](https://www.tensorflow.org/) - Deep learning framework

### WorkFlow of Docbot
Docbot basically works by the utility of the following three modules:-
* Deep learning Module - This is the Module in which the training and the Evaluation phase is done
* Android Studio Module - This is the Module in which the Front End of the Android chatbot is created
* Flask API Module - This is the Module which connects the data from the front end of the bot(X-Ray) to the backend of the bot(Learned Model) to make predictions

The system architecture of this project is given below:-

![Flip flop Networks Architecture](architecture.jpg)

Initially the training was done on 5215 Normal and Pneumonial Chest X-Ray Images.For the Training of the Model, Transfer Learning was utilized.We imported the weights of the VGG16 Pre Trained model and used it for the training phase.Post training we saved the model in the h5 format. Evaluation of the trained model was done on 624 test images, Recall being the most important evaluation metric for this particular problem statement was 97% and the F1 score was 90%.The confusion matrix screenshot image can be seen at "conf_matrix.png".This learned model(model.h5) was then imported in the Python flask API script to make the predictions on the incoming data(X-Ray image) from the client side(Android Bot).Thus the X-Ray Image is sent through the Chatbot by the POST method, the Chest X-ray Image is then read in the server script and then the learned model(model.h5) file is loaded and the prediction is made.The result of the prediction is then sent to the client side through the Response method.The architecture of the complete system is given in the "architecture.jpg" file.

#### Credits:- 
The Android bot module was done by my teamate Sairam during the hackathon.

